#include <WiFi.h>
#include <WiFiMulti.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include <SPI.h>
#include <TFT_eSPI.h>       // Hardware-specific library
TFT_eSPI tft = TFT_eSPI();  // Invoke custom library
int scanTime = 5; //In seconds
BLEScan* pBLEScan;
WiFiMulti WiFiMulti;
int temperature, humidity;

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      if ( advertisedDevice.getAddress().toString() == "58:2d:34:3b:7d:3c" ) {
        uint8_t* payload = advertisedDevice.getPayload();
        uint8_t *type_of_data;
        for ( int i = 0; i < advertisedDevice.getPayloadLength(); i++ ) {
          if (payload[i] == 0xfe) {
            if (payload [i - 1] == 0x95) {
              type_of_data    = &payload [i + 12];

              if (advertisedDevice.getPayloadLength() == 25 || advertisedDevice.getPayloadLength() == 51)
              {
                temperature = (int) (((payload[i + 16] & 0xff) << 8) + (payload[i + 15] & 0xff));
                humidity = (int)(((payload[i + 18] & 0xff) << 8) + (payload[i + 17] & 0xff));
                Serial.printf("Temperature : %d.%d°C     \n", temperature / 10, temperature % 10);
                Serial.printf("Humidity : %d.%d%%     \n", humidity / 10, humidity % 10);
              }
              else if (advertisedDevice.getPayloadLength() == 23 || advertisedDevice.getPayloadLength() == 49)
              {
                if (*type_of_data == 0x6) {
                  humidity = (int)(((payload[i + 18] & 0xff) << 8) + (payload[i + 17] & 0xff));
                  Serial.printf("Humidity : %d.%d%%     \n", humidity / 10, humidity % 10);
                } else if (*type_of_data == 0x4) {
                  temperature = (int) (((payload[i + 16] & 0xff) << 8) + (payload[i + 15] & 0xff));
                  Serial.printf("Temperature : %d.%d°C     \n", temperature / 10, temperature % 10);
                }
              }
              else if (advertisedDevice.getPayloadLength() == 22 || advertisedDevice.getPayloadLength() == 48)
              {
                continue;
              }
              else
              {
                for (i = i; i < advertisedDevice.getPayloadLength(); i++) {
                  Serial.printf("%02x ", payload[i + 1]);
                }
              }
            }
          }
        }
      }
    }
};

void setup() {

  tft.init();  
  tft.fillScreen(TFT_BLACK);

  //////////////
  // BLE SCAN //
  //////////////
  Serial.begin(115200);
  Serial.println("Scanning...");

  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan(); //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);  // less or equal setInterval value

  ///////////////
  // WIFI SETUP//
  ///////////////
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network
  WiFiMulti.addAP("ASUS", "");
  Serial.print("Waiting for WiFi... ");

  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(500);
}

void loop() {
  //////////////
  // BLE SCAN //
  //////////////
  BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
  pBLEScan->clearResults();

  ///////////////
  // AFFICHAGE //
  ///////////////

  tft.fillScreen(TFT_BLACK);
  tft.setCursor(0, 0, 4);

  tft.setTextColor(TFT_WHITE, TFT_BLACK);
  tft.println("Donnees\n");

  tft.println("Temp : ");
  float ftemp = (float)temperature/10;
  tft.println(String(ftemp,1)+ (char*)" °C");

  tft.println("Humid : ");
  float fhum = (float)humidity/10;
  tft.println(String(fhum,1)+ (char*)"%");

  ///////////////
  // WIFI LOOP //
  ///////////////
  const uint16_t port = 80;
  const char * host = "192.168.1.15"; // ip or dns
  const char* route = "/UPDATE";

  Serial.print("Connecting to ");
  Serial.println(host);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;

  if (!client.connect(host, port)) {
    Serial.println("Connection failed.");
    Serial.println("Waiting 5 seconds before retrying...");
    delay(5000);
    return;
  }

  // This will send a request to the server
  String url = "GET ";
  url += route;
  url += "?t=";
  url += temperature;
  url += "&h=";
  url += humidity;
  url += " HTTP/1.1\n\n";
  client.print(url);
  Serial.println(url);

  int maxloops = 0;

  //wait for the server's reply to become available
  while (!client.available() && maxloops < 2000)
  {
    maxloops++;
    delay(1); //delay 1 msec
  }
  if (client.available() > 0)
  {
    //read back one line from the server
    String line = client.readStringUntil('\r');
    Serial.println(line);
  }
  else
  {
    Serial.println("client.available() timed out ");
  }

  Serial.println("Closing connection.");
  client.stop();

  Serial.println("Waiting 5 seconds before restarting...");
  delay(5000);
}
