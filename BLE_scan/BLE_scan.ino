/*
   Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleScan.cpp
   Ported to Arduino ESP32 by Evandro Copercini
*/

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

int scanTime = 5; //In seconds
BLEScan* pBLEScan;

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      if ( advertisedDevice.getAddress().toString() == "58:2d:34:3b:7d:3c" ) {
        uint8_t* payload = advertisedDevice.getPayload();
        uint8_t* frame_control, *data_id, *index, *MAC_address, *type_of_data, *data_length;
        float temperature, humidity, battery;
        for ( int i = 0; i< advertisedDevice.getPayloadLength(); i++ ) {
          if (payload[i] == 0xfe) {
            if (payload [i-1] == 0x95) {
              frame_control   = &payload [i+1];
              data_id         = &payload [i+3];
              index           = &payload [i+5];
              MAC_address     = &payload [i+6];
              type_of_data    = &payload [i+12];
              data_length     = &payload [i+14];

              if (advertisedDevice.getPayloadLength()==25 || advertisedDevice.getPayloadLength()==51)
              {
                temperature = (float) (((payload[i+16]&0xff)<<8) + (payload[i+15]&0xff));
                humidity = (float)(((payload[i+18]&0xff)<<8) + (payload[i+17]&0xff));
                Serial.printf("Temperature : %.1f°C     \n",temperature/10);
                Serial.printf("Humidity : %.1f%%     \n",humidity / 10);
              }
              else if (advertisedDevice.getPayloadLength()==23|| advertisedDevice.getPayloadLength()==49)
              {
                if (*type_of_data == 0x6) {
                  humidity = (float) (((payload[i+16]&0xff)<<8) + (payload[i+15]&0xff));
                  Serial.printf("Humidity : %.1f%%     \n",humidity / 10);
                } else if (*type_of_data == 0x4) {
                  temperature = (float) (((payload[i+16]&0xff)<<8) + (payload[i+15]&0xff));
                  Serial.printf("Temperature : %.1f°C     \n",temperature / 10);
                }
              }
              else if (advertisedDevice.getPayloadLength()==22|| advertisedDevice.getPayloadLength()==48)
              {
                continue;
              }
              else
              {
                for(i=i;i<advertisedDevice.getPayloadLength();i++){
                  Serial.printf("%02x ",payload[i+1]);
                }
              }
            }
          }
        }
      }
    }
};

void setup() {
  Serial.begin(115200);
  Serial.println("Scanning...");

  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan(); //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);  // less or equal setInterval value
}

void loop() {
  // put your main code here, to run repeatedly:
  BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
  //Serial.print("Devices found: ");
  //Serial.println(foundDevices.getCount());
  //Serial.println("Scan done!");
  pBLEScan->clearResults();   // delete results fromBLEScan buffer to release memory
  delay(2000);
}
